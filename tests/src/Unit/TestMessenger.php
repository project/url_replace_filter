<?php

namespace Drupal\Tests\url_replace_filter\Unit;

use Drupal\Core\Messenger\MessengerInterface;

/**
 * Stub implementation of MessengerInterface for unit tests.
 */
class TestMessenger implements MessengerInterface {

  /**
   * {@inheritDoc}
   */
  public function addMessage($message, $type = self::TYPE_STATUS, $repeat = FALSE) {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function addStatus($message, $repeat = FALSE) {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function addError($message, $repeat = FALSE) {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function addWarning($message, $repeat = FALSE) {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function all() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function messagesByType($type) {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function deleteAll() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function deleteByType($type) {
    return [];
  }

}
