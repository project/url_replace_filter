<?php

namespace Drupal\Tests\url_replace_filter\Unit;

use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Stub implementation of RouteMatchInterface for unit tests.
 */
class TestRouteMatch implements RouteMatchInterface {

  /**
   * {@inheritDoc}
   */
  public function getRouteName() {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getRouteObject() {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getParameter($parameter_name) {
    return new ParameterBag();
  }

  /**
   * {@inheritDoc}
   */
  public function getParameters() {
    return new ParameterBag();
  }

  /**
   * {@inheritDoc}
   */
  public function getRawParameter($parameter_name) {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getRawParameters() {
    return new ParameterBag();
  }

}
