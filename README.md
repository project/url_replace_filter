# URL Replace Filter Drupal module.
## Description

The URL Replace Filter module allows administrators to replace the base URL in
`<img>` and `<a>` elements.

Users tend to create links and images in their content with absolute URLs. This
can be a problem if the site moves to another domain (perhaps between
development and production sites) or is behind a proxy, with a different address
for authenticated users.

If you need to use the module on the now unsupported Drupal 8 core,
you can remain on version 8.x-1.0: this version is only for Drupal 9 and later.


### Some replacement examples

* Link
  * Before: `<a href="http://example.com:8080/somepath">Some link</a>`
  * After: `<a href="/some-path">Some link</a>`
* Image
  * Before: `<img src="http://a.example.com/files/img.jpg" alt="Some image" />`
  * After: `<img src="/files/img.jpg" alt="Some image" />`

You can setup such replacements in the URL Replace Filter settings as follow:

* Link
  * Original: `http://example.com:8080/`
  * Replacement: `%baseurl/`
* Image
  * Original: `http://dev.example.com/`
  * Replacement: `%baseurl/`

`%baseurl` is a token for your site's base URL. The above examples assume a site
located in the domain's root directory (in which case `%baseurl` is actually
empty).

Like any Drupal filter, the original user-entered content is not altered. The
filter is only applied (and its result cached) when the node is viewed.


## Installation

1. Enable the module

2. Go to the `Configuration` > `Content authoring` > `Text formats and editors`
   page, and click configure next to the text format that shall replace URLs.

3. Enable the _URL Replace Filter_ in the text format's configuration page, and
   save the configuration.

4. Click the `URL Replace filter` vertical tab at the bottom of the page.
   In the URL Replace Filter box, enter original and replacement URLs in the
   appropriate fields and save the configuration. More empty replacement fields
   will automatically be added after saving, in case you need more fields than
   provided by default.

## Contributing to development

Contributions are welcome. Be sure that your patches or merge requests pass both
PHPstan, PHPCS and PHPUnit tests.

- Install phpstan-drupal.
- Install drupal/core-dev.
- Copy `url_replace_filter.phpunit.xml` to your project root,
  and fill in the missing info for dev host URL and database.
  Adjust if needed based on your local setup and the current state of
  `web/core/phpunit.xml.dist`
- Create the `web/sites/default/simpletest/browser_output` directory,
  if it does not exist yet.
- Run module tests
  `phpunit --config=$PWD/phpunit.xml -v web/modules/contrib/url_replace_filter`
- Run PHPstan from your project root:
```
phpstan                                          \
  --configuration=web/modules/contrib/url_replace_filter/phpstan.neon \
  analyze --level=9                              \
  web/modules/contrib/url_replace_filter
```
- Run PHPCodeSniffer

      phpcs                                                           \
        --standard=Drupal,DrupalPractice                              \
        --extensions=php,module,install,profile,theme,css,info,md,yml \
        web/modules/contrib/url_replace_filter`
